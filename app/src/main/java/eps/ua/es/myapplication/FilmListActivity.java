package eps.ua.es.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

public class FilmListActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    FilmAdapter mAdapter;

    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);

        mRecyclerView = (RecyclerView)findViewById(R.id.filmsList);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new FilmAdapter(FilmDataSource.films);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemListener(new FilmAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Film f, int position) {
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_INDEX, position);
                startActivity(intent);
            }
        });
    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);
        mRecyclerView = (RecyclerView)findViewById(R.id.filmsList);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new FilmAdapter(FilmDataSource.films);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemListener(new FilmAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Film f, int position) {
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_INDEX, position);
                startActivity(intent);
            }
        });


        /*ArrayAdapter<Film> adapter = new FilmAdapter(this, R.layout.item_film, FilmDataSource.films);
        setListAdapter(adapter);*/
    }
    /*@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
        intent.putExtra(FilmDataActivity.EXTRA_FILM_INDEX, position);
        startActivity(intent);
    }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.miNuevaPelicula:
                nuevaPelicula();
                return true;
            case R.id.miAcercaDe:
                abrirAcercaDe();
                return true;
        }
        return false;
    }

    private void abrirAcercaDe() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void nuevaPelicula() {
        Film f = new Film();
        FilmDataSource.films.add(f);

        mAdapter.notifyItemInserted(FilmDataSource.films.size()-1);
    }

    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                intent.putExtra("EXTRA_FILM_TITLE", "Pelicula a");
                startActivity(intent);

            }
        });
        /*findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                startActivity(intent);
            }
        });*/
        /*findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                intent.putExtra("EXTRA_FILM_TITLE", "pelicula b");
                startActivity(intent);
            }
        });
        findViewById(R.id.button6).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(FilmListActivity.this, AboutActivity.class); startActivity(intent);

            }
        });

        /*getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String elemento = (String) getListAdapter().getItem(i);
            }
        });
    }*/
}
/*public class FilmListActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<Film> adapter = new ArrayAdapter<Film>(this, android.R.layout.simple_list_item_1, FilmDataSource.films);
        setListAdapter(adapter);
    }

}*/
