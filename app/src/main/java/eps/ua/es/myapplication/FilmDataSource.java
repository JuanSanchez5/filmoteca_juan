package eps.ua.es.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 10/10/17.
 */

public class FilmDataSource {
    public static List<Film> films;

    static{
        films = new ArrayList<Film>();

        Film f = new Film();

        f.title = "Regreso al futuro";
        f.director = "Robert Zemeckis";
        f.imageResId = R.mipmap.ic_launcher;
        f.coments = "";
        f.format = Film.FORMAT_DIGITAL;
        f.genre = Film.GENRE_SCIFI;
        f.imdbUrl = "http://www.imdb.com/title/tt0088763";
        f.year = 1985;
        films.add(f);

        f = new Film();

        f.title = "300";
        f.director = "Zack Snyder";
        f.imageResId = R.mipmap.ic_launcher;
        f.coments = "";
        f.format = Film.FORMAT_BLURAY;
        f.genre = Film.GENRE_ACTION;
        f.imdbUrl = "http://www.imdb.com/title/tt0088763";
        f.year = 2006;
        films.add(f);

        f = new Film();
        f.title = "Invictus";
        f.director = "Clint Eastwood";
        f.imageResId = R.mipmap.ic_launcher;
        f.coments = "";
        f.format = Film.FORMAT_DVD;
        f.genre = Film.GENRE_DRAMA;
        f.imdbUrl = "http://www.imdb.com/title/tt0088763";
        f.year = 2009;
        films.add(f);
    }

}
