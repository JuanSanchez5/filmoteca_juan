package eps.ua.es.myapplication;

/**
 * Created by mastermoviles on 10/10/17.
 */

public class Film {
    public final static int FORMAT_DVD = 0;
    public final static int FORMAT_BLURAY = 1;
    public final static int FORMAT_DIGITAL = 2;

    public final static int GENRE_ACTION = 0;
    public final static int GENRE_COMEDY = 1;
    public final static int GENRE_DRAMA = 2;
    public final static int GENRE_SCIFI = 3;
    public final static int GENRE_HORROR = 4;

    public int imageResId;
    public String title;
    public String director;
    public int year;
    public int genre;
    public int format;
    public String imdbUrl;
    public String coments;

    public String toString(){
        return title;
    }


}
