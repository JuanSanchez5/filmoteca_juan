package eps.ua.es.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.net.Uri;

public class FilmDataActivity extends AppCompatActivity {

    public static final String EXTRA_FILM_TITLE = "EXTRA_FILM_TITLE";
    public static final String EXTRA_FILM_INDEX = "EXTRA_FILM_INDEX";

    private static final int EDIT = 1;
    String title = "";
    int filmIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_data);
        Intent intent = getIntent();
        filmIndex = getIntent().getIntExtra(EXTRA_FILM_INDEX, -1);
        title = intent.getStringExtra("EXTRA_FILM_TITLE");
                TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(title);

        findViewById(R.id.button8).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(FilmDataActivity.this, FilmEditActivity.class);
                startActivityForResult(intent, EDIT);
            }
        });
        findViewById(R.id.button9).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(FilmDataActivity.this, FilmListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        findViewById(R.id.showInImdb).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.imdb.com/title/tt0088763"));
                startActivity(intent);
                // Code here executes on main thread after user presses button
                /*Intent intent = new Intent(FilmDataActivity.this, FilmListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/

            }
        });

        updateData();


    }
    private void updateData() {
        TextView title = (TextView) findViewById(R.id.textView4);
        TextView year = (TextView) findViewById(R.id.textView8);
        TextView director = (TextView) findViewById(R.id.textView6);
        //TextView txtComentarios = (TextView) findViewById(R.id.txtDataComments);
        TextView txtFormatoGenero = (TextView) findViewById(R.id.textView9);
        //ImageView imgPoster = (ImageView) findViewById(R.id.imageView3);

        Film film = FilmDataSource.films.get(filmIndex);

        title.setText(film.title);
        director.setText(film.director);
        year.setText("" + film.year);
        //txtComentarios.setText(film.comments);
        /*txtFormatoGenero.setText(getResources().getStringArray(R.array.formats)[film.format] +
                ", " + getResources().getStringArray(R.array.genres)[film.genre]);*/
        //imgPoster.setImageResource(film.imageResId);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == EDIT && resultCode == RESULT_OK) {
            TextView txtTitulo = (TextView) findViewById(R.id.textView2);
            title = title + " modificado";
            txtTitulo.setText(title);
        }
    }
}
