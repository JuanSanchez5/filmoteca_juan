package eps.ua.es.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mastermoviles on 9/11/17.
 */

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder> {

    private List<Film> Films;

    public FilmAdapter(List<Film> films) {
        Films = films;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_film, parent, false);
        final ViewHolder holder = new ViewHolder(v);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if(mListener!=null) {
                    mListener.onItemClick(Films.get(position), position);
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(Films.get(position));
    }

    @Override
    public int getItemCount() {
        return Films.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView director;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);
            title = (TextView)v.findViewById(R.id.textViewTitle);
            director = (TextView)v.findViewById(R.id.textViewDirector);
            image = (ImageView)v.findViewById(R.id.imageView4);
        }

        public void bind(Film f) {
            title.setText(f.title);
            director.setText(f.director);
            image.setImageResource(f.imageResId);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Film f, int position);
    }
    private OnItemClickListener mListener;

    public void setOnItemListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
